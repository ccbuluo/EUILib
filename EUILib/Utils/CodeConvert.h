#pragma once
#include "../EUILib.h"

class CUnicode2Ansi
{
public:
	CUnicode2Ansi();
	~CUnicode2Ansi(void);

	char* GetAnsiString(const WCHAR *pUnicodeStr);
	void Release();
private:
	char *m_pBuf;
	int m_nBufLen;
};

class CAnsi2Unicode
{
public:
	CAnsi2Unicode();
	~CAnsi2Unicode(void);

	WCHAR* GetUnicodeString(const char *pAnsiStr);
	void Release();
private:
	WCHAR *m_pBuf;
	int m_nBufLen;
};
