#pragma once
#include "../EUILib.h"

class EULIB_API ShareMemoryBase
{
public:
	ShareMemoryBase(LPCTSTR pName, UINT uSize, DWORD dwAccess = PAGE_READWRITE );
	~ShareMemoryBase(void);

	void* GetShareMemoryBuffer();
	int GetErrorCode() const;

	void Lock();
	void UnLock();

private:
	HANDLE 	m_hMapFile;
	void*	m_buffer;
	int		m_errCode;
	HANDLE  m_Mutex;
};
