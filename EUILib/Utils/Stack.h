/************************************************************************/
/* Name ：       堆栈模版类
/* Description : 堆栈默认大小为128
/* Note :        默认情况下允许进行值覆盖，可以通过EnableOverlap(BOOL bEnable)进行控制
/* Author :      Victor
/* Crete Time :  2014.6.16
/************************************************************************/
#pragma once

#include <windows.h>

static enum
{
	STACK_OK = 0,
	STACK_FULL,
	STACK_EMPTY,
	STACK_NONE,
};

template <typename T> 

class CStack
{
public:

	CStack(UINT length = 128);

	~CStack(void);

public:

	LRESULT Push(const T &Items);

	LRESULT Pop(T* Items);

	void EnableOverlap(BOOL bEnable);

private:

	T* m_pStack;

	UINT m_nTop;

	BOOL m_bEnableOverlap;

	UINT m_nLength;
};

template <typename T>
CStack<T>::CStack( UINT length /*= 128*/ ) : m_nLength(0), m_nTop(0), m_pStack(NULL), m_bEnableOverlap(TRUE)
{
	if( length > 0 )
	{
		m_pStack = new T[length];
		m_nLength = length;
	}
}


template <typename T>
CStack<T>::~CStack( void )
{
	if( m_pStack != NULL )
	{
		delete[] m_pStack;
		m_pStack = NULL;
	}
}


template <typename T>
void CStack<T>::EnableOverlap( BOOL bEnable )
{
	m_bEnableOverlap = bEnable;
}

template <typename T>
LRESULT CStack<T>::Pop( T* Items )
{
	if(m_pStack == NULL)
	{
		return STACK_NONE;
	}

	if(m_nTop == 0)
	{
		return STACK_EMPTY;
	}

	m_nTop--;
	*Items = m_pStack[m_nTop];
	
	return STACK_OK;
}

template <typename T>
LRESULT CStack<T>::Push( const T &Items )
{
	if( m_pStack == NULL )
	{
		return STACK_NONE;
	}

	if( m_nTop == m_nLength )
	{
		if(!m_bEnableOverlap)
		{
			return STACK_FULL;
		}
		else
		{
			memmove(m_pStack,m_pStack+1,sizeof(T)*(m_nLength-1));
			m_nTop--;
		}
	}

	m_pStack[m_nTop] = Items;
	m_nTop++;

	return STACK_OK;
}
