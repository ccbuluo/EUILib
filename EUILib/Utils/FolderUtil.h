/********************************************************************
*   copyright: 
*   file name: FolderUtil 
*      author: victor
* create time: 2014/10/14
*     version: v0.1
*
* description: 提供与文件夹操作相关的简单接口
*********************************************************************/
#pragma once

#include "../EUILib.h"

class CFolderUtil
{
public:
	CFolderUtil(void);
	~CFolderUtil(void);

	/****************************************************
	* Method:   CFolderUtil::GetFolderFileCount
	* Describe:  获取指定目录下文件的个数（不包括隐藏文件及文件夹）
	* Access:   public static 
	* @param:   strFolderPath  文件夹路径（绝对路径）
	* @param:   bRecursion   是否递归查找子目录
	* @return:  UINT  返回文件个数
	****************************************************/
	static UINT GetFolderFileCount(LPCTSTR strFolderPath, BOOL bRecursion = FALSE);

	/****************************************************
	* Method:   CFolderUtil::OpenFolderDialog
	* Describe: 打开路径选中对话框 
	* Access:   public static 
	* @param:   hWnd 打开的对话框的父窗口句柄
	* @param:   strPath  输出参数 返回打开的目录路径
	* @param:   strStartPath 浏览的起始路径
	* @return:  void
	****************************************************/
	static void OpenFolderDialog(HWND hWnd, LPTSTR strPath, LPCTSTR strStartPath = NULL);

	/****************************************************
	* Method:   CFolderUtil::IsFolderEmpty
	* Describe: 判断文件夹是否为空（忽略隐藏的文件及文件夹）
	* Access:   public static 
	* @param:   strPath 指定文件夹路径
	* @return:  BOOL
	****************************************************/
	static BOOL IsFolderEmpty(LPCTSTR strFolderPath);

	/****************************************************
	* Method:   CFolderUtil::IsFolderExist
	* Describe: 判断文件夹是否存在
	* Access:   public static 
	* @param:   strPath 指定文件夹路径
	* @return:  BOOL
	****************************************************/
	static BOOL IsFolderExist(LPCTSTR strPath);

};


