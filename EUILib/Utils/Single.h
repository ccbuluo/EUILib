#pragma once

#include "SingleObjKiller.h"

extern CSingleObjKiller g_SingleObjKiller;

#define SINGLEOBJ_DELCARE(T)   friend class SingleObj<T>; 
#define SINGLEOBJ(T)           SingleObj<T>::GetInstance()
#define SINGLEOBJ_DESTROY(T)    SingleObj<T>::Destroy()

template<typename T>
class SingleObj
{
private:
	static T * mInstance;

public:
	static void Destroy()
	{
		if (NULL != mInstance)
		{
			delete mInstance;
			mInstance = NULL;
		}
	}

public:
	static T * GetInstance();
};

template<typename T>
T * SingleObj<T>::mInstance = NULL;

template<typename T>
T * SingleObj<T>::GetInstance()
{
	if(NULL == mInstance)
	{
		mInstance = new T;
		g_SingleObjKiller.AddObj(Destroy);
	}
	return mInstance ;
}
