#include "FolderUtil.h"
#include "ShlObj.h"

CFolderUtil::CFolderUtil( void )
{
}

CFolderUtil::~CFolderUtil( void )
{
}

UINT CFolderUtil::GetFolderFileCount( LPCTSTR strFolderPath, BOOL bRecursion/* = FALSE*/ )
{
	WIN32_FIND_DATA fd;
	UINT uTotalCount = 0;
	TCHAR strPath[MAX_PATH] = {0};
	TCHAR strNewPath[MAX_PATH] = {0};
	
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	wcscpy_s(strPath, MAX_PATH, strFolderPath);
	wcscat_s(strPath, MAX_PATH, L"\\*.*");

	HANDLE hSearch = FindFirstFile(strPath, &fd);
	if (hSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			if(_tcscmp(fd.cFileName,_T(".")) != 0 && _tcscmp(fd.cFileName,_T("..")) != 0
				&& (fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0)
			{
				if(bRecursion && (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					wcscpy_s(strNewPath, MAX_PATH, strFolderPath);
					wcscat_s(strNewPath, MAX_PATH, L"\\");
					wcscat_s(strNewPath, MAX_PATH, fd.cFileName);
					uTotalCount  = uTotalCount + GetFolderFileCount(strNewPath,bRecursion);
				}

				uTotalCount++;
			}
		} while(FindNextFile(hSearch, &fd));
		FindClose(hSearch);
	}

	return uTotalCount;
}

int CALLBACK BrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
{
	switch(uMsg)
	{
	case BFFM_INITIALIZED:
		{
			::SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
			break;
		}
	case BFFM_SELCHANGED:
		{
			TCHAR curr[MAX_PATH];   
			SHGetPathFromIDList((LPCITEMIDLIST)lParam, curr);   
			::SendMessage(hwnd, BFFM_SETSTATUSTEXT,0, (LPARAM)curr);   
		}
		break;
	default:
		break;
	}

	return 0;   
}

void CFolderUtil::OpenFolderDialog( HWND hWnd, LPTSTR strPath, LPCTSTR strStartPath/*= NULL*/ )
{
	BROWSEINFO info;
	//TCHAR szPath[MAX_PATH]={0};
	ZeroMemory(&info, sizeof(BROWSEINFO));
	info.hwndOwner = hWnd;
	info.lpszTitle = _T("请选择一个文件夹:");
	info.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE | BIF_STATUSTEXT;
	info.lpfn = BrowseCallbackProc;
	/*TCHAR tcharBuf[MAX_PATH] = {0};
	_tcscpy(tcharBuf,strPath);*/
	info.lParam = (long)(strStartPath);/*(long)(&tcharBuf);*/
	ITEMIDLIST *pItem;
	pItem = SHBrowseForFolder(&info);
	if(pItem)
	{  
		SHGetPathFromIDList(pItem, strPath);
		GlobalFree(pItem);
	}
}

BOOL CFolderUtil::IsFolderEmpty( LPCTSTR strFolderPath )
{
	WIN32_FIND_DATA fd;
	BOOL bRet = TRUE;
	TCHAR strPath[MAX_PATH] = {0};

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	wcscpy_s(strPath, MAX_PATH, strPath);
	wcscat_s(strPath, MAX_PATH, L"\\*.*");

	HANDLE hSearch = FindFirstFile(strPath, &fd);
	if (hSearch != INVALID_HANDLE_VALUE)
	{
		BOOL bFind = TRUE;
		while(bFind)
		{
			if(_tcscmp(fd.cFileName,_T(".")) != 0 && _tcscmp(fd.cFileName,_T("..")) != 0)
			{
				if((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0)
				{
					bRet = FALSE;
					break;
				}
			}

			bFind = FindNextFile(hSearch, &fd);
		}
		FindClose(hSearch);
	}

	return bRet;
}

BOOL CFolderUtil::IsFolderExist( LPCTSTR strPath )
{
	return (::GetFileAttributes(strPath) != INVALID_FILE_ATTRIBUTES);
}

