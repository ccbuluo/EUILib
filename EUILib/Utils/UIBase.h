#pragma once
#include "../EUILib.h"

#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))

//-------------------------CPoint---------------------------
class EULIB_API CPoint : public tagPOINT
{
public:
	CPoint();
	CPoint(const POINT& src);
	CPoint(int x, int y);
	CPoint(LPARAM lParam);
};

//-------------------------CSize---------------------------
class EULIB_API CSize : public tagSIZE
{
public:
	CSize();
	CSize(const SIZE& src);
	CSize(const RECT rc);
	CSize(int cx, int cy);
};

//-------------------------CRect---------------------------
class EULIB_API CRect : public tagRECT
{
public:
	CRect();
	CRect(const RECT& src);
	CRect(int iLeft, int iTop, int iRight, int iBottom);

	int GetWidth() const;
	int GetHeight() const;
	void SetWidth(int nWidth);
	void SetHeight(int nHeight);
	void Empty();
	bool IsNull() const;
	void Join(const RECT& rc);
	void ResetOffset();
	void Normalize();
	void Offset(int cx, int cy);
	void Inflate(int cx, int cy);
	void Deflate(int cx, int cy);
	void Union(CRect& rc);
};
