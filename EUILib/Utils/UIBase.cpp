#include "UIBase.h"

//-------------------------CPoint---------------------------
CPoint::CPoint()
{
	x = y = 0;
}

CPoint::CPoint( const POINT& src )
{
	x = src.x;
	y = src.y;
}

CPoint::CPoint( int _x, int _y )
{
	x = _x;
	y = _y;
}

CPoint::CPoint( LPARAM lParam )
{
	x = GET_X_LPARAM(lParam);
	y = GET_Y_LPARAM(lParam);
}
//-------------------------CPoint---------------------------

//-------------------------CSize---------------------------
CSize::CSize()
{
	cx = cy = 0;
}

CSize::CSize( const SIZE& src )
{
	cx = src.cx;
	cy = src.cy;
}

CSize::CSize( const RECT rc )
{
	cx = rc.right - rc.left;
	cy = rc.bottom - rc.top;
}

CSize::CSize( int _cx, int _cy )
{
	cx = _cx;
	cy = _cy;
}
//-------------------------CSize---------------------------

//-------------------------CRect---------------------------
CRect::CRect()
{
	left = top = right = bottom = 0;
}

CRect::CRect(const RECT& src)
{
	left = src.left;
	top = src.top;
	right = src.right;
	bottom = src.bottom;
}

CRect::CRect(int iLeft, int iTop, int iRight, int iBottom)
{
	left = iLeft;
	top = iTop;
	right = iRight;
	bottom = iBottom;
}

int CRect::GetWidth() const
{
	return right - left;
}

int CRect::GetHeight() const
{
	return bottom - top;
}

void CRect::SetWidth( int nWidth )
{
	right = left + nWidth;
}

void CRect::SetHeight( int nHeight )
{
	bottom = top + nHeight;
}

void CRect::Empty()
{
	left = top = right = bottom = 0;
}

bool CRect::IsNull() const
{
	return (left == 0 && right == 0 && top == 0 && bottom == 0); 
}

void CRect::Join(const RECT& rc)
{
	if( rc.left < left ) left = rc.left;
	if( rc.top < top ) top = rc.top;
	if( rc.right > right ) right = rc.right;
	if( rc.bottom > bottom ) bottom = rc.bottom;
}

void CRect::ResetOffset()
{
	::OffsetRect(this, -left, -top);
}

void CRect::Normalize()
{
	if( left > right ) { int iTemp = left; left = right; right = iTemp; }
	if( top > bottom ) { int iTemp = top; top = bottom; bottom = iTemp; }
}

void CRect::Offset(int cx, int cy)
{
	::OffsetRect(this, cx, cy);
}

void CRect::Inflate(int cx, int cy)
{
	::InflateRect(this, cx, cy);
}

void CRect::Deflate(int cx, int cy)
{
	::InflateRect(this, -cx, -cy);
}

void CRect::Union(CRect& rc)
{
	::UnionRect(this, this, &rc);
}



//-------------------------CRect---------------------------
