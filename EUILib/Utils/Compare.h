#pragma once

template <typename T>
BOOL StructEqual( const T& struct0, const T& struct1 )
{
	return (memcmp(reinterpret_cast<const VOID *>(&struct0), reinterpret_cast<const VOID *>(&struct1), sizeof(T) ) == 0);
}
