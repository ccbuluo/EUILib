#pragma once

#include "DataArray.h"

typedef void (*Destructor)();

	class CSingleObjKiller
	{
	public:
		CSingleObjKiller(void);
		~CSingleObjKiller(void);

		void AddObj(Destructor fun);

	public:

		CDataArray<Destructor> m_Array;
	};