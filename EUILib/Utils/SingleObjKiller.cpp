#include "SingleObjKiller.h"

CSingleObjKiller g_SingleObjKiller;

CSingleObjKiller::CSingleObjKiller(void)
{
}

CSingleObjKiller::~CSingleObjKiller(void)
{
	Destructor fun;
	for(UINT i = 0; i < m_Array.GetSize(); i++)
	{
		fun = (Destructor)(m_Array[i]);
		fun();
	}
}

void CSingleObjKiller::AddObj( Destructor fun )
{
	m_Array.Add(fun);
}
