#pragma once
#include "../EUILib.h"

class EULIB_API CExString
{
public:
	enum { MAX_LOCAL_STRING_LEN = 63 };

	CExString();
	CExString(const TCHAR ch);
	CExString(const CExString& src);
	CExString(LPCTSTR lpsz, int nLen = -1);
	~CExString();

	void Empty();
	int GetLength() const;
	bool IsEmpty() const;
	TCHAR GetAt(int nIndex) const;
	void Append(LPCTSTR pstr);
	void Assign(LPCTSTR pstr, int nLength = -1);
	LPCTSTR GetData() const;

	void SetAt(int nIndex, TCHAR ch);
	operator LPCTSTR() const;

	TCHAR operator[] (int nIndex) const;
	const CExString& operator=(const CExString& src);
	const CExString& operator=(const TCHAR ch);
	const CExString& operator=(LPCTSTR pstr);
#ifdef _UNICODE
	const CExString& CExString::operator=(LPCSTR lpStr);
	const CExString& CExString::operator+=(LPCSTR lpStr);
#else
	const CExString& CExString::operator=(LPCWSTR lpwStr);
	const CExString& CExString::operator+=(LPCWSTR lpwStr);
#endif
	CExString operator+(const CExString& src) const;
	CExString operator+(LPCTSTR pstr) const;
	const CExString& operator+=(const CExString& src);
	const CExString& operator+=(LPCTSTR pstr);
	const CExString& operator+=(const TCHAR ch);

	bool operator == (LPCTSTR str) const;
	bool operator != (LPCTSTR str) const;
	bool operator <= (LPCTSTR str) const;
	bool operator <  (LPCTSTR str) const;
	bool operator >= (LPCTSTR str) const;
	bool operator >  (LPCTSTR str) const;

	int Compare(LPCTSTR pstr) const;
	int CompareNoCase(LPCTSTR pstr) const;

	void MakeUpper();
	void MakeLower();

	CExString Left(int nLength) const;
	CExString Mid(int iPos, int nLength = -1) const;
	CExString Right(int nLength) const;

	int Find(TCHAR ch, int iPos = 0) const;
	int Find(LPCTSTR pstr, int iPos = 0) const;
	int ReverseFind(TCHAR ch) const;
	int Replace(LPCTSTR pstrFrom, LPCTSTR pstrTo);

	int __cdecl Format(LPCTSTR pstrFormat, ...);
	int __cdecl SmallFormat(LPCTSTR pstrFormat, ...);

protected:
	LPTSTR m_pstr;
	TCHAR m_szBuffer[MAX_LOCAL_STRING_LEN + 1];
};
