#include "CodeConvert.h"

CUnicode2Ansi::CUnicode2Ansi() : m_pBuf(NULL),m_nBufLen(0)
{
}

CUnicode2Ansi::~CUnicode2Ansi( void )
{
	Release();
}

char* CUnicode2Ansi::GetAnsiString( const WCHAR *pUnicodeStr )
{
	if (pUnicodeStr == NULL) return NULL;
	
	int nLen = WideCharToMultiByte(CP_ACP, 0, pUnicodeStr, -1, NULL, 0, NULL, NULL);
	if (nLen > 0)
	{
		if (nLen > m_nBufLen)
		{
			Release();
			m_nBufLen = nLen;
			m_pBuf = new char[m_nBufLen + 1];
		}
		memset(m_pBuf, 0, m_nBufLen+1);
		WideCharToMultiByte(CP_ACP, 0, pUnicodeStr, -1, m_pBuf, nLen, NULL, NULL);
	}
	else
	{
		return NULL;
	}
	
	return m_pBuf;
}

void CUnicode2Ansi::Release()
{
	if (m_pBuf != NULL)
	{
		delete[] m_pBuf;
		m_pBuf = NULL;
		m_nBufLen = 0;
	}
}

CAnsi2Unicode::CAnsi2Unicode() : m_pBuf(NULL),m_nBufLen(0)
{

}

CAnsi2Unicode::~CAnsi2Unicode( void )
{
	Release();
}

WCHAR* CAnsi2Unicode::GetUnicodeString( const char *pAnsiStr )
{
	if(pAnsiStr == NULL) return NULL;

	int nLen = MultiByteToWideChar(CP_ACP, 0, pAnsiStr, -1, NULL, 0);
	if (nLen > 0)
	{
		if (nLen > m_nBufLen)
		{
			Release();
			m_nBufLen = nLen;
			m_pBuf = new WCHAR[m_nBufLen + 1];
		}
		memset(m_pBuf, 0, m_nBufLen+1);
		MultiByteToWideChar(CP_ACP, 0, pAnsiStr, -1, m_pBuf, nLen);
	}
	else
	{
		return NULL;
	}

	return m_pBuf;
}

void CAnsi2Unicode::Release()
{
	if (m_pBuf != NULL)
	{
		delete[] m_pBuf;
		m_pBuf = NULL;
		m_nBufLen = 0;
	}
}
