/************************************************************************/
/* Name ：       循环队列模版类 
/* Description : 队列默认大小为128
/* Note :        默认情况下允许进行队列值覆盖，可以通过EnableOverlap(BOOL bEnable)进行控制
/* Author :      Victor
/* Crete Time :  2014.6.16
/************************************************************************/
#pragma once

#include <windows.h>

enum
{
	QUEUE_OK = 0,
	QUEUE_FULL,/**< 队列已满 */
	QUEUE_EMPTY,/**< 队列为空 */
	QUEUE_NONE,/**< 队列没有初始化 */
};

template <typename T> 

class CLoopQueue
{
public:

	CLoopQueue(UINT length = 128);

	~CLoopQueue(void);

public:

	HRESULT EnQueue(const T& Items);

	HRESULT DeQueue(T *Items);

	void EnableOverlap(BOOL bEnable);

private:

	UINT inline GetNextIndex(UINT Index);

private:

	T *m_pQueue;

	UINT m_nHead;

	UINT m_nTail;

	UINT m_nLength;

	BOOL m_bEnableOverlap;

	BYTE m_u8State;

};

template <typename T>
CLoopQueue<T>::CLoopQueue( UINT length /*= 128*/ ) : m_nLength(0), m_pQueue(NULL), m_nHead(0),
m_nTail(0), m_bEnableOverlap(TRUE), m_u8State(QUEUE_EMPTY)
{
	if( length > 0 )
	{
		m_pQueue = new T[length];
		m_nLength = length;
	}
}

template <typename T>
CLoopQueue<T>::~CLoopQueue( void )
{
	if ( m_pQueue != NULL )
	{
		delete[] m_pQueue;
		m_pQueue = NULL;
	}
}


template <typename T>
UINT CLoopQueue<T>::GetNextIndex(UINT Index)
{
	return ( ++Index ) % m_nLength;
}

template <typename T>
HRESULT CLoopQueue<T>::DeQueue( T *Items )
{
	if( m_pQueue == NULL )
	{
		return QUEUE_NONE;
	}

	if(m_u8State == QUEUE_EMPTY)
	{
		return QUEUE_EMPTY;
	}

	*Items = m_pQueue[m_nTail];

	m_nTail = GetNextIndex(m_nTail);

	if( m_u8State == QUEUE_FULL )
	{
		m_u8State = QUEUE_OK;
	}

	if(m_nTail == m_nHead)
	{
		m_u8State = QUEUE_EMPTY;
	}

	return QUEUE_OK;
}

template <typename T>
HRESULT CLoopQueue<T>::EnQueue( const T& Items )
{
	if( m_pQueue == NULL )
	{
		return QUEUE_NONE;
	}

	if( m_u8State == QUEUE_FULL)
	{
		if(!m_bEnableOverlap)
		{
			return QUEUE_FULL;
		}
		else
		{
			m_nTail = GetNextIndex(m_nTail);
		}
	}

	m_pQueue[m_nHead] = Items;

	if( m_u8State == QUEUE_EMPTY )
	{
		m_u8State = QUEUE_OK;
	}

	m_nHead = GetNextIndex(m_nHead);

	if( m_nHead == m_nTail )
	{
		m_u8State = QUEUE_FULL;
	}

	return QUEUE_OK;
}

template <typename T>
void CLoopQueue<T>::EnableOverlap( BOOL bEnable )
{
	m_bEnableOverlap = bEnable;
}
