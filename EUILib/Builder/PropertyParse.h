#pragma once
#include "../Utils/ExString.h"
#include "../Utils/UIBase.h"
//enum ePropertyType
//{
//	PROPERTY_INT,
//	PROPERTY_BOOL,
//	PROPERTY_DOUBLE,
//	PROPERTY_SIZE,
//	PROPERTY_RECT,
//	PROPERTY_COLOR,
//	PROPERTY_STRING,
//	PROPERTY_WSTRING,
//};

class CPropertyParse
{
public:
	CPropertyParse(void);
	~CPropertyParse(void);

	static BOOL GetInt(LPCTSTR pData, int& OutData, int Default = 0);
	static BOOL GetBOOL(LPCTSTR pData, BOOL& OutData, BOOL Default = FALSE);
	static BOOL GetDouble(LPCTSTR pData, DOUBLE& OutData, DOUBLE Default = 0.0);
	static BOOL GetSize(LPCTSTR pData, CSize& OutData);
	static BOOL GetRect(LPCTSTR pData, CRect& OutData);
	static BOOL GetColor(LPCTSTR pData, COLORREF& OutData, COLORREF Default = 0);
	static BOOL GetString(LPCTSTR pData, CExString& OutData);
};
