#include "StdAfx.h"
#include "PropertyParse.h"

CPropertyParse::CPropertyParse(void)
{
}

CPropertyParse::~CPropertyParse(void)
{
}

BOOL CPropertyParse::GetInt( LPCTSTR pData, int& OutData, int Default/* = 0*/)
{
	if (pData != NULL && _tcslen(pData) > 0)
	{
		OutData = _tstoi(pData);
		return TRUE;
	}

	OutData = Default;

	return FALSE;
}

BOOL CPropertyParse::GetBOOL( LPCTSTR pData, BOOL& OutData, BOOL Default/*= FALSE*/ )
{
	if(pData != NULL && _tcslen(pData) > 0)
	{
		if (_tcsicmp(pData, L"0") == 0 || _tcsicmp(pData, L"FALSE") == 0)
		{
			OutData = FALSE;
		}
		else
		{
			OutData = TRUE;
		}

		return TRUE;
	}

	OutData = Default;

	return FALSE;
}

BOOL CPropertyParse::GetDouble( LPCTSTR pData, DOUBLE& OutData, DOUBLE Default/* = 0.0*/ )
{
	if(pData != NULL && _tcslen(pData) > 0)
	{
		OutData = _tstof(pData);
		return TRUE;
	}

	OutData = Default;

	return FALSE;
}

BOOL CPropertyParse::GetSize( LPCTSTR pData, CSize& OutData )
{
	if(pData != NULL && _tcslen(pData) > 0)
	{
		_stscanf(pData, L"%d,%d", OutData.cx, OutData.cy);
		return TRUE;
	}

	OutData.cx = OutData.cy = 0;
	
	return FALSE;
}

BOOL CPropertyParse::GetRect( LPCTSTR pData, CRect& OutData )
{
	if(pData != NULL && _tcslen(pData) > 0)
	{
		_stscanf(pData, L"%d,%d,%d,%d", OutData.left, OutData.top, OutData.right, OutData.bottom);
		OutData.right = OutData.left + OutData.right;
		OutData.bottom = OutData.top + OutData.bottom;
		return TRUE;
	}

	OutData.left = OutData.top = OutData.right = OutData.bottom = 0;

	return FALSE;
}

BOOL CPropertyParse::GetColor( LPCTSTR pData, COLORREF& OutData, COLORREF Default/* = 0*/ )
{
	if(pData != NULL && _tcslen(pData) > 0)
	{
		TCHAR *pstrValue = pData;
		if( *pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
		DWORD clrColor = _tcstoul(pstrValue, NULL, 16);
		return TRUE;
	}

	OutData = Default;

	return FALSE;
}

BOOL CPropertyParse::GetString( LPCTSTR pData, CExString& OutData )
{
	if(pData != NULL)
	{
		OutData = pData;
	}

	return FALSE;
}
