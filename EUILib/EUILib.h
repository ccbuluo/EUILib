#pragma once

#ifdef  EUILIB_EXPORTS
#define EULIB_API  __declspec(dllexport)
#else
#define EULIB_API  __declspec(dllimport)
#endif

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <assert.h>
#include <malloc.h>

#define ASSERT assert
