#include "StdAfx.h"
#include "FontManager.h"
#include "FontCreater.h"

CFontManager::CFontManager(void)
{
}

CFontManager::~CFontManager(void)
{
}

HFONT CFontManager::GetFontByIndex( int nIndex )
{
	int nCount = m_FontArray.GetSize();
	if (nIndex < nCount)
	{
		return m_FontArray[nIndex];
	}

	return NULL;
}

BOOL CFontManager::CreateFont( CMarkupNode& Node )
{
	CFontCreater FontCreater;
	HFONT hFont = FontCreater.CreateFont(Node);
	AddFont(hFont);

	return (hFont != NULL);
}

void CFontManager::AddFont( HFONT hFont )
{
	if (hFont != NULL)
	{
		m_FontArray.Add(hFont);
	}
}

int CFontManager::FindFont( HFONT hFont )
{
	int nCount = m_FontArray.GetSize();
	for (int i = 0; i < nCount; i++)
	{
		if (m_FontArray[i] == hFont)
		{
			return i;
		}
	}

	return -1;
}
