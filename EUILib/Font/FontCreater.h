#pragma once
#include "../Utils/ExString.h"
#include "../Utils/UIMarkup.h"

class CFontCreater
{
public:
	CFontCreater(void);
	~CFontCreater(void);

	HFONT CreateFont(CMarkupNode& Node);

private:
	CExString m_FontName;

	UINT m_FontSize;
	BOOL m_bBold;
	BOOL m_bUnderLine;
	BOOL m_bItalic;
	BYTE m_Quality;
	BYTE m_CharSet;

};
