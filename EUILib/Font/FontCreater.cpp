#include "StdAfx.h"
#include "FontCreater.h"
#include "../Builder/PropertyParse.h"

CFontCreater::CFontCreater(void):
m_FontSize(16),
m_bBold(FALSE),
m_bUnderLine(FALSE),
m_bItalic(FALSE)
{
}

CFontCreater::~CFontCreater(void)
{
}

HFONT CFontCreater::CreateFont(CMarkupNode& Node)
{
	CPropertyParse::GetString(Node.GetAttributeValue(L"Name"), m_FontName);
	CPropertyParse::GetInt(Node.GetAttributeValue(L"Size"), m_FontSize, 16);
	CPropertyParse::GetBOOL(Node.GetAttributeValue(L"Bold"), m_bBold);
	CPropertyParse::GetBOOL(Node.GetAttributeValue(L"Underline"), m_bUnderLine);
	CPropertyParse::GetBOOL(Node.GetAttributeValue(L"Italic"), m_bItalic);
	CPropertyParse::GetInt(Node.GetAttributeValue(L"Quality"), m_Quality, DEFAULT_QUALITY);
	CPropertyParse::GetInt(Node.GetAttributeValue(L"CharSet"), m_CharSet, DEFAULT_CHARSET);
	
	if (m_FontName.GetLength() > 0)
	{
		LOGFONT lf = { 0 };
		_tcscpy(lf.lfFaceName, pStrFontName);
		lf.lfCharSet = m_CharSet;
		lf.lfHeight = -nSize;
		if( m_bBold ) lf.lfWeight += FW_BOLD;
		if( m_bUnderLine ) lf.lfUnderline = TRUE;
		if( m_bItalic ) lf.lfItalic = TRUE;
		lf.lfQuality = m_Quality;
		HFONT hFont = ::CreateFontIndirect(&lf);
		
		return hFont;
	}

	return NULL;
}
