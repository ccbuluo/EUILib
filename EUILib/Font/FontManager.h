#pragma once
#include "../Utils/DataArray.h"
#include "../Utils/UIMarkup.h"

class CFontManager
{
public:
	CFontManager(void);
	~CFontManager(void);

	HFONT GetFontByIndex(int nIndex);
	BOOL CreateFont(CMarkupNode& Node);

	void AddFont(HFONT hFont);
	int FindFont(HFONT hFont);
private:
	CDataArray<HFONT> m_FontArray;
};