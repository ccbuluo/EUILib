#pragma once
#include "../EUILib.h"

#define WND_NAME_LEN   64

class CWndBase
{
public:
	CWndBase();
	virtual ~CWndBase();
	virtual BOOL ShowWindow(BOOL bShow);
	virtual BOOL Create(HWND hWndParent, LPCTSTR &strWndClass, LPCTSTR &strWndName);	
	virtual BOOL CreateEx(HWND hWndParent,LPCTSTR &strWndClass, LPCTSTR &strWndName,DWORD dwStyle);
	BOOL SetParentWindow(HWND hWndParent);	
	HWND GetWindow(void) const;
	HWND GetParentWindow(void) const;
protected:
	virtual LRESULT WndProc(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam);	
	virtual void GetDataForRegistryClass(WNDCLASS &wc);
	virtual void OnLButtonDown(UINT nFlags, POINT point);
	virtual void OnLButtonUp(UINT nFlags, POINT point);
private:
	HWND m_hWnd;
	mutable HWND m_hWndParent;
	TCHAR m_strWndClass[WND_NAME_LEN];
	TCHAR m_strWndName[WND_NAME_LEN];
	DWORD m_dwStyle;

	BOOL RegisterClass();
	BOOL CreateWnd(DWORD dwStyle,DWORD dwExStyle);
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam);
};
