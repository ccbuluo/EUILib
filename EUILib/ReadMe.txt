﻿EUILib是一套用于嵌入式设备的UI库，旨在降低嵌入式设备HMI开发过程中的开发成本;同时，该UI库中开封装了部分常用的工具类。
通过使用该UI库，可以轻松的实现UI与逻辑直接的解耦，实现UI与逻辑的分离，提高开发效率。
该UI库采用XML(后续支持json，yaml)，作为UI配置文件，资源问题采用zip文件，也可以是一个DLL文件。
该UI库提供了多种UI渲染方式,可以根据实际硬件情况及系统要求进行选择，目前该库预计会支持GDI,DDraw,Opengl es。

文件目录结构说明：
EUILib
     | Core          核心控制
     | Control       控件
     | Animation     动画
     | Images        图片加载
     | Builder       UI生成，解析UI配置文件，生成UI数据结构
     | View          View类
     | Render        渲染(图片，文字，基本形状，动画支持)
     | Utils         通用工具类
     | Window        窗口