#pragma once

#define  LOG_LEVEL 4 //LOG输出等级数参考eLogGoalsType
#define  USER_LOG_GOAL  LOG_GOALS_INFO
#define  USER_LOG_TYPE  LOG_TYPE_UART_AND_FILE

#if (LOG_LEVEL < 4)
#include "Log/Log.h"
#define LOGE  CLogFile::GetLog()->E
#define LOGSAVE()  CLogFile::GetLog()->SaveToFile()
#define LOGINIT  CLogFile::InitLog
#endif

#if (LOG_LEVEL < 3)
#define LOGW  CLogFile::GetLog()->W
#endif

#if (LOG_LEVEL < 2)
#define LOGD  CLogFile::GetLog()->D
#endif

#if (LOG_LEVEL < 1)
#define LOGI  CLogFile::GetLog()->I
#endif

#ifndef LOGI
#define LOGI(i)
#endif

#ifndef LOGD
#define LOGD(i)
#endif

#ifndef LOGW
#define LOGW(i)
#endif

#ifndef LOGE
#define LOGE(i)
#endif

#ifndef LOGSAVE()
#define LOGSAVE()
#endif

#ifndef LOGINIT
#define LOGINIT(i)
#endif

