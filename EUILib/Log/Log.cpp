#include "StdAfx.h"
#include "Log.h"

#define  FORMAT_LOG_STRING(level)\
	char LogBuf[LOG_TEXT_BUFFER_LENGTH] = {0};\
	strcpy(LogBuf, level);\
	strcat(LogBuf, m_chTag);\
	int nLen = strlen(LogBuf);\
	va_list args;\
	va_start(args, format);\
	vsprintf_s(LogBuf + nLen, LOG_TEXT_BUFFER_LENGTH - nLen , format, args);\
	va_end(args);\
	DoLogOutput(LogBuf)


CLogBase::CLogBase( const char *pTag, eLogGoalsType nGoalType /*= LOG_GOALS_INFO*/ ) :
m_nLogGoalType(LOG_GOALS_INFO)
{
	memset(m_chTag,0,TAG_TEXT_LENGHT);
	if (pTag != NULL)
	{
		strcpy(m_chTag, pTag);
	}
	else
	{
		strcpy(m_chTag, "[Log]");
	}
}

CLogBase::~CLogBase( void )
{

}

void CLogBase::I(const CHAR *format, ...)
{
	if (m_nLogGoalType <= LOG_GOALS_INFO)
	{
		FORMAT_LOG_STRING("{I}-");
	}
}

void CLogBase::D(const CHAR *format, ...)
{
	if (m_nLogGoalType <= LOG_GOALS_DEBUG)
	{
		FORMAT_LOG_STRING("{D}-");
	}
}

void CLogBase::W(const CHAR *format, ...)
{
	if (m_nLogGoalType <= LOG_GOALS_WARNING)
	{
		FORMAT_LOG_STRING("{W}-");
	}
}

void CLogBase::E(const CHAR *format, ...)
{
	if (m_nLogGoalType <= LOG_GOALS_ERROR)
	{
		FORMAT_LOG_STRING("{E}-");
	}
}

void CLogBase::DoLogOutput(const CHAR *pLogInfo)
{
	printf(pLogInfo);
}

//---------------------------------------------------------

CLogFile::CLogFile( const char *pTag, 
				   const WCHAR* pFileName, 
				   eLogGoalsType nGoalType /*= LOG_GOALS_INFO*/,
				   eLogOutputType nOutputType /*= LOG_TYPE_UART*/) 
: CLogBase(pTag, nGoalType),
m_pFileLogBuf(NULL),
m_nFileBufLength(0),
m_nLogOutputType(LOG_TYPE_UART)
{
	memset(&m_chLogFilePath, 0, MAX_PATH);
	if (pFileName != NULL)
	{
		wcscpy_s(m_chLogFilePath, MAX_PATH - 1, pFileName);
		if (GetFileAttributes(m_chLogFilePath) != INVALID_FILE_ATTRIBUTES)
		{
			DeleteFile(m_chLogFilePath);
		}
	}
	m_nLogOutputType = nOutputType;
	m_pFileLogBuf = new char[LOG_FILE_BUFFEE_LENGTH];
	ClearLogBuffer();

#ifdef ENABLE_PRINT_TIME_IN_FILE
	char chDatatime[64] = {0};
	SYSTEMTIME sTime;
	GetLocalTime(&sTime);
	sprintf_s(chDatatime, 64, "Create Log At:%d-%02d-%02d %02d:%02d:%02d\r\n", 
		sTime.wYear, sTime.wMonth, sTime.wDay, 
		sTime.wHour, sTime.wMinute, sTime.wSecond);
	strcat(m_pFileLogBuf, chDatatime);
	m_nFileBufLength += strlen(m_pFileLogBuf);
#endif
}

CLogFile::~CLogFile()
{
	if (m_pFileLogBuf != NULL)
	{
		delete[] m_pFileLogBuf;
		m_pFileLogBuf = NULL;
	}
}

void CLogFile::DoLogOutput( const CHAR *pLogInfo )
{
	if (m_nLogOutputType == LOG_TYPE_UART || m_nLogOutputType == LOG_TYPE_UART_AND_FILE)
	{
		printf(pLogInfo);
	}
	
	if (m_nLogOutputType == LOG_TYPE_FILE || m_nLogOutputType == LOG_TYPE_UART_AND_FILE)
	{
		AddLogToFileLogBuffer(pLogInfo);
	}
}

void CLogFile::InitLog( const char *pTag,
						   const WCHAR* pFileName, 
						   eLogGoalsType nGoalType/* = LOG_GOALS_INFO*/,
						   eLogOutputType nOutputType /*= LOG_TYPE_UART */)
{
	static CAutoRelease s_AutoRelease;
	if (m_pInstance != NULL)
	{
		delete m_pInstance;
	}
	m_pInstance = new CLogFile(pTag, pFileName, nGoalType, nOutputType);
	s_AutoRelease.SetLogFile(m_pInstance);
}

CLogFile* CLogFile::GetLog()
{
	if (m_pInstance == NULL)
	{
		InitLog("[LOG] ", NULL, LOG_GOALS_INFO, LOG_TYPE_UART);
	}

	return m_pInstance;
}

void CLogFile::AddLogToFileLogBuffer( const char* pLog )
{
	int nLen = strlen(pLog);

#ifdef ENABLE_PRINT_TIME_IN_FILE
	char chDatatime[32] = {0};
	SYSTEMTIME sTime;
	GetLocalTime(&sTime);
	sprintf_s(chDatatime, 32, "[%02d:%02d:%02d]-", sTime.wHour, sTime.wMinute, sTime.wSecond);
	nLen += strlen(chDatatime);
#endif

	if (nLen + m_nFileBufLength > LOG_FILE_BUFFEE_LENGTH)//buffer��
	{
		WriteLogBufferToFile();
		ClearLogBuffer();
	}

#ifdef ENABLE_PRINT_TIME_IN_FILE
	strcat(m_pFileLogBuf, chDatatime);
#endif

	strcat(m_pFileLogBuf, pLog);
	m_nFileBufLength += nLen;
}

void CLogFile::WriteLogBufferToFile()
{
	if (m_chLogFilePath[0] != '\0')
	{
		HANDLE hFile = CreateFile(m_chLogFilePath, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile != INVALID_HANDLE_VALUE)
		{
			int nSize = GetFileSize(hFile, NULL);
			if (nSize > 0)
			{
				SetFilePointer(hFile, 0, NULL, FILE_END);
			}
			
			DWORD dwRet;
			WriteFile(hFile, m_pFileLogBuf, m_nFileBufLength, &dwRet, NULL);
			CloseHandle(hFile);
		}
	}
}

void CLogFile::ClearLogBuffer()
{
	memset(m_pFileLogBuf, 0, LOG_FILE_BUFFEE_LENGTH);
	m_nFileBufLength = 0;
}

void CLogFile::SaveToFile()
{
	if (m_nFileBufLength > 0)
	{
		WriteLogBufferToFile();
		ClearLogBuffer();
	}
}

CLogFile* CLogFile::m_pInstance = NULL;


CAutoRelease::CAutoRelease() :
m_pInstance(NULL)
{
}

CAutoRelease::~CAutoRelease( void )
{
	if(m_pInstance != NULL)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}
}

void CAutoRelease::SetLogFile( CLogFile* pInstance )
{
	if (pInstance != NULL)
	{
		m_pInstance = pInstance;
	}
}
