#pragma once

enum eLogGoalsType
{
	LOG_GOALS_INFO = 0,
	LOG_GOALS_DEBUG,
	LOG_GOALS_WARNING,
	LOG_GOALS_ERROR,

	LOG_GOALS_NONE,
};

enum eLogOutputType
{
	LOG_TYPE_UART,
	LOG_TYPE_FILE,
	LOG_TYPE_UART_AND_FILE,
};

//是否在LOG文件中打印时间信息
#define ENABLE_PRINT_TIME_IN_FILE

class CLog
{
public:
	virtual void I(const CHAR *format, ...) = 0;//普通信息输出
	virtual void D(const CHAR *format, ...) = 0;//调试信息输出
	virtual void W(const CHAR *format, ...) = 0;//警告信息输出
	virtual void E(const CHAR *format, ...) = 0;//错误信息输出
};


#define  TAG_TEXT_LENGHT            32
#define  LOG_TEXT_BUFFER_LENGTH     256


class CLogBase : public CLog
{
public: 

	CLogBase(const char *pTag, eLogGoalsType nGoalType = LOG_GOALS_INFO);

	~CLogBase(void);

	virtual void I(const CHAR *format, ...);

	virtual void D(const CHAR *format, ...);

	virtual void W(const CHAR *format, ...);

	virtual void E(const CHAR *format, ...);

protected:

	virtual void DoLogOutput(const CHAR *pLogInfo);

private:

	char m_chTag[TAG_TEXT_LENGHT];

	eLogGoalsType m_nLogGoalType;
};

#define LOG_FILE_BUFFEE_LENGTH    1024 * 512

class CLogFile : public CLogBase
{
public:
	CLogFile( const char *pTag, 
		const WCHAR* pFileName, 
		eLogGoalsType nGoalType = LOG_GOALS_INFO,
		eLogOutputType nOutputType = LOG_TYPE_UART );

	~CLogFile();

	virtual void DoLogOutput( const CHAR *pLogInfo );

	static void InitLog(const char *pTag,
		const WCHAR* pFileName, 
		eLogGoalsType nGoalType = LOG_GOALS_INFO,
		eLogOutputType nOutputType = LOG_TYPE_UART );

	static CLogFile* GetLog();

	void SaveToFile();
private:

	void AddLogToFileLogBuffer(const char* pLog);
	void WriteLogBufferToFile();
	void ClearLogBuffer();

private:
	WCHAR m_chLogFilePath[MAX_PATH];
	char* m_pFileLogBuf;
	int m_nFileBufLength;
	eLogOutputType m_nLogOutputType;
	static CLogFile* m_pInstance;
};

class CAutoRelease
{
public:
	CAutoRelease();
	~CAutoRelease(void);

	void SetLogFile(CLogFile* pInstance);
	
private:
	CLogFile* m_pInstance;

};