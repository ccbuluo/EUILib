#pragma once

#define SINGLE_OBJECT(obj) \
	static obj##& GetInstance()\
	{\
	static obj sobj;\
	return sobj;\
	}

#ifndef PROPERTY
#define PROPERTY(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void);\
public: virtual void set##funName(varType var);
#endif

#ifndef PROPERTY_DEFAULT
#define PROPERTY_DEFAULT(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void) {return varName;}\
public: virtual void set##funName(varType var) {varName = var;}
#endif