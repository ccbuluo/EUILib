#include "StdAfx.h"
#include "UIControl.h"
#include "../Builder/PropertyParse.h"

CUIControl::CUIControl(void)
{
}

CUIControl::~CUIControl(void)
{
}

void CUIControl::Builder( CMarkupNode& Node )
{
	CPropertyParse::GetRect(Node.GetAttributeValue(L"CtrlRect"), m_CtrlRect);
	CPropertyParse::GetString(Node.GetAttributeValue(L"Name"), m_CtrlName);
}
