#pragma once
#include "../Utils/UIMarkup.h"
#include "../Utils/UIBase.h"
#include "../Utils/ExString.h"
#include "../MacroDefine.h"

class CUIControl
{
public:
	CUIControl(void);
	~CUIControl(void);

public:
	void Builder(CMarkupNode& Node);

public:
	PROPERTY_DEFAULT(CRect, m_CtrlRect, CtrlRect)
	PROPERTY_DEFAULT(CExString, m_CtrlName, CtrlName)
};
